package work

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
)

type Work struct{}

// Run - this is were the work is done or other function calls
//
//	this implements the Runner interface in the timer package
func (w *Work) Run(ctx context.Context, errChan chan error) error {
	log.Println("Running")
	url := fmt.Sprintf("%v", ctx.Value("url"))
	response, err := http.Get(url)
	if err != nil {
		errChan <- err
		return err
	}

	defer response.Body.Close()

	log.Println("Run Complete")

	b, err := io.ReadAll(response.Body)
	if err != nil {
		errChan <- err
		return err
	}

	log.Println(string(b))
	return nil

}
