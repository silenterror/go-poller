package timer

import (
	"context"
	"errors"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Runner - the Runner interface to implement this will be ran and will be the actual work segment.
type Runner interface {
	Run(ctx context.Context, errChan chan error) error
}

// Orchestrator - the Orchestrator interface defining the Orchestrate method
type Orchestrator interface {
	Orchestrate(f Runner, ctx context.Context)
}

// timedWorker - associated Run method and internal heartbeat
type timedWorker struct {
	id    int
	heart heartbeat
	// channel used to store a signal to trap interrupts
	sigChan chan os.Signal
	// channel to receive errors on
	errChan chan error
}

// heart - provides the ticker and total ticks passed
type heartbeat struct {
	timer      *time.Ticker
	totalTicks int
}

var ()

// New - takes an id int and a heartRate in milliseconds.
//
//	returns a new worker with an embedded heartbeat
func New(id int, heartRate int64) (Orchestrator, error) {
	if heartRate <= 0 {
		return nil, errors.New("duration needs to be a positive integer")
	}
	return &timedWorker{
		id:      id,
		heart:   heartbeat{timer: time.NewTicker(time.Millisecond * time.Duration(heartRate)), totalTicks: 0},
		sigChan: make(chan os.Signal),
		errChan: make(chan error),
	}, nil
}

// Orchestrate - orchestrates the process of watching the worker heartbeat
//
//	while also execution of the workers Run method at every beat.
func (w *timedWorker) Orchestrate(f Runner, ctx context.Context) {
	go f.Run(ctx, w.errChan)
	for {
		signal.Notify(w.sigChan, os.Interrupt, syscall.SIGTERM)

		select {
		case <-w.heart.timer.C:
			w.heart.totalTicks++
			go f.Run(ctx, w.errChan)
		case sig := <-w.sigChan:
			log.Printf("totalTicks: %d", w.heart.totalTicks)
			log.Printf("signal %v received, exiting...:", sig)
			// Graceful Exit
			w.heart.timer.Stop()
			close(w.sigChan)
			close(w.errChan)
			os.Exit(0)
		case e := <-w.errChan:
			log.Println(e)
		}
	}
}
