# go-poller

## Use Case

To poll an endpoint for data on a regular cadence.  Furtherore, this could
be modified to run a task or other action on a heartbeat.


### Getting Started

Clone the repository
```sh
git@gitlab.com:silenterror/go-poller.git
```

Run the application
```sh
go run cmd/main.go
```
