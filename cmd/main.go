package main

import (
	"context"

	"gitlab.com/silenterror/tickerwork/pkg/timer"
	"gitlab.com/silenterror/tickerwork/pkg/work"
)

var (
	url = "http://api.mathjs.org/v4/?expr=2%2F3&precision=8"
)

func main() {

	// create worker 1 with a heartrate of 500ms
	worker, err := timer.New(1, 5000)
	if err != nil {
		panic(err)
	}

	// creating a context for instance supplying a url to fetch
	ctx := context.Background()
	ctx = context.WithValue(ctx, "url", url)

	// Start the work with the worker all the custom logic that is needing run
	// on a schedule is in the work package specifically the Run method located there

	w := work.Work{}

	// pass the work which implements Runner and the context to Orchestrate
	worker.Orchestrate(&w, ctx)

}
